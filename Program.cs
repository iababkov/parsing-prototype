﻿using OpenQA.Selenium;
using Parser.Utils;
using Parser.WebPages;
using Parser.Settings;
using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;

namespace Parser
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8; // Cyrillic output

            //var shopPage = PageFactory.Instance.Get<MVideoPage>();
            var shopPage = PageFactory.Instance.Get<EldoradoPage>();

            shopPage.Open();

            if (!shopPage.IsSearchFieldPresent)
            {
                Console.WriteLine("Поисковая строка недоступна");
            }

            shopPage.SearchField.SendKeys(MainSettings.SearchStringText);

            shopPage.SearchField.SendKeys(Keys.Enter);

            Console.WriteLine("На первой странице по заданному товару были найдены следующие цены:");

            var priceTextSet = new HashSet<decimal>();

            int attemptsCounter = 5;

            while (attemptsCounter > 0)
            {
                try
                {
                    var prices = shopPage.GetAllPricesParagraphs();

                    foreach (var price in prices)
                    {
                        Console.WriteLine(price.Text);

                        if (decimal.TryParse(price.Text.Replace(" ", ""), out decimal result))
                        {
                            priceTextSet.Add(result);
                        }
                    }

                    attemptsCounter = 0;
                }
                catch (StaleElementReferenceException)
                {
                    attemptsCounter--;

                    Thread.Sleep(500);
                }
            }

            if (priceTextSet.Count > 0)
            {
                Console.WriteLine(priceTextSet.Min());
                Console.WriteLine(priceTextSet.Max());
            }

            shopPage.Close(); // not abstract Close method
        }
    }
}
