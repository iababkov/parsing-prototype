﻿namespace Parser.Settings
{
    public static class MainSettings
    {
        public static string MVideoPageUrl { get; } = "https://www.mvideo.ru/";
        public static string EldoradoPageUrl { get; } = "https://www.eldorado.ru/";

        public static double PageLoad { get; } = 60000;

        public static double ImplicitWait { get; } = 5000;

        public static double AsyncJS { get; } = 3000;

        public static int ShortTimeout { get; } = 3000;

        public static string SearchStringText { get; } = "iPhone 12";
    }
}
