﻿using OpenQA.Selenium;
using Parser.Settings;
using Parser.Timeouts;
using System.Collections.Generic;

namespace Parser.WebPages
{
    public class EldoradoPage : AbstractPage
    {
        public EldoradoPage(IWebDriver driver) : base(driver) { }

        public IWebDriver Driver
        {
            get
            {
                CustomTimeout.ShortWait();
                return _driver;
            }

            set => _driver = value;
        }

        public bool IsSearchFieldPresent => IsElementPresent(By.Name("search"));

        public IWebElement SearchField => FindByName("search");

        public IEnumerable<PageElement> GetAllPricesParagraphs()
        {
            var paragraphs = GetAllParagraphsByCSS("span.JP.QP");

            foreach (var paragraph in paragraphs)
            {
                string priceText = paragraph.Text;

                if (priceText != "")
                {
                    yield return new PageElement() { Text = priceText };
                }
            }
        }

        public void Open()
        {
            Driver.Url = MainSettings.EldoradoPageUrl;
        }

        public void Close()
        {
            Driver.Close();
        }
    }

    public class PageElement
    {
        public string Text;
    }
}
