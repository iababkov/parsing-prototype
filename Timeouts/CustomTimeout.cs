﻿using Parser.Settings;
using System.Threading;

namespace Parser.Timeouts
{
    public static class CustomTimeout
    {
        public static void ShortWait()
        {
            Thread.Sleep(MainSettings.ShortTimeout);
        }
    }
}
